# What are Researchers using 3d Data for?

#### Research methods and skills

Learning to work efficiently with 3D data allows researchers to develop and enhance fundamental research methods.

Research methods are the systematic tools used to find, collect, analyse and interpret information, the methods by which you conduct research into a subject or a topic.

_link to sage interactive map for research methods here_  
  
The two main research methods that benefit from 3d data are Data Visualisation and Prototyping.



#### Data visualisation

_**Expand these points into text:** document, share, collaborate, communicate, teach, allow reproducible research_

  
The examples you see here are examples of 3D scanning, photogrammetry, 3D rendering

#### Prototyping

_**Expand these points into text:**_ **experimental methods, iteration, fail fast to succeed faster**

_3d printing, grasshopper plugins for thermal design etc._



