# Table of contents

* [Researcher Compute Services](README.md)

## 3d Data

* [\*master text from slides](3d-data/slides.md)
* [What is 3d Data?](3d-data/what-is-3d-data.md)
* [Why are researchers using 3d Data?](3d-data/why-are-researchers-using-3d-data.md)
* [What are Researchers using 3d Data for?](3d-data/what-are-researchers-using-3d-data-for.md)
* [Examples of researchers using 3d Data](3d-data/examples-of-researchers-using-3d-data/README.md)
  * [Prototyping and ancient horn with 3d printing and scanning](3d-data/examples-of-researchers-using-3d-data/prototyping-and-ancient-horn-with-3d-printing-and-scanning.md)

## Tools

* [What tool is right for you?](tools/what-tool-is-right-for-you/README.md)
  * [Virtual Creation](tools/what-tool-is-right-for-you/virtual-creation.md)
  * [Physical to Virtual](tools/what-tool-is-right-for-you/physical-to-virtual.md)
  * [Virtual to Physical](tools/what-tool-is-right-for-you/virtual-to-physical.md)
* [3d Design](tools/3d-design.md)
* [3d Scanning](tools/3d-scanning.md)
* [3d Printing](tools/3d-printing.md)

## Learning Material

* [TinkerCAD](learning-material/tinkercad/README.md)
  * [What is TinkerCAD?](learning-material/tinkercad/what-is-tinkercad.md)
  * [The workplane and navigation](learning-material/tinkercad/the-workplane-and-navigation.md)
  * [Basic Shapes](learning-material/tinkercad/basic-shapes.md)
  * [Design Challenge!](learning-material/tinkercad/design-challenge.md)
  * [Wrap up and summary](learning-material/tinkercad/wrap-up-and-summary.md)
  * [Cheat sheet and other Resources](learning-material/tinkercad/cheat-sheet-and-other-resources.md)
* [Rhino3d](learning-material/rhino3d.md)
* [Fusion 360](learning-material/fusion-360.md)

